#!/bin/bash

if [ -n "$1" ]
	then
		package=${PWD##*/}_${1}
	else
		package=${PWD##*/}
fi

if [ -f ${package}.zip ]
	then
		rm -f ${package}.zip
fi

gulp build

if [ -n "$1" ]
	then
		git archive -o ${package}.zip $1
	else
		git archive -o ${package}.zip HEAD
fi
zip -r ${package}.zip Resources/Public/Stylesheets/*.css Resources/Public/Stylesheets/*.map
