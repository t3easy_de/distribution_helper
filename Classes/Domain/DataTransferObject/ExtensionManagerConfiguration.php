<?php
namespace T3easy\DistributionHelper\Domain\DataTransferObject;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Jan Kiesewetter <jan@t3easy.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ExtensionManagerConfiguration
 *
 */
class ExtensionManagerConfiguration
{
    /**
     * @var array
     */
    protected $packageCategories = [];

    /**
     * @var array
     */
    protected $packageKeys = [];

    /**
     * @var array
     */
    protected $packageKeysToExclude = [];

    /**
     * @param array $configuration
     */
    public function __construct(array $configuration)
    {
        foreach ($configuration as $key => $value) {
            if (property_exists(__CLASS__, $key)) {
                $setter = 'set' . ucfirst($key);
                $this->$setter($value);
            }
        }
    }

    /**
     * @param string $packageCategories
     * @return void
     */
    public function setPackageCategories($packageCategories)
    {
        $this->packageCategories = GeneralUtility::trimExplode(',', $packageCategories);
    }

    /**
     * @return array
     */
    public function getPackageCategories()
    {
        return $this->packageCategories;
    }

    /**
     * @param string $packageKeys
     * @return void
     */
    public function setPackageKeys($packageKeys)
    {
        $this->packageKeys = GeneralUtility::trimExplode(',', $packageKeys);
    }

    /**
     * @return array
     */
    public function getPackageKeys()
    {
        return $this->packageKeys;
    }

    /**
     * @param string $packageKeysToExclude
     * @return void
     */
    public function setPackageKeysToExclude($packageKeysToExclude)
    {
        $this->packageKeysToExclude = GeneralUtility::trimExplode(',', $packageKeysToExclude);
    }

    /**
     * @return array
     */
    public function getPackageKeysToExclude()
    {
        return $this->packageKeysToExclude;
    }
}
