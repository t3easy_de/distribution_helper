<?php
namespace T3easy\DistributionHelper\Backend\View\BackendLayout;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Jan Kiesewetter <jan@t3easy.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use T3easy\DistributionHelper\Utility\ExtensionManagerConfigurationUtility;
use T3easy\DistributionHelper\Utility\PageTSconfigUtility;
use TYPO3\CMS\Backend\View\BackendLayout\BackendLayout;
use TYPO3\CMS\Backend\View\BackendLayout\BackendLayoutCollection;
use TYPO3\CMS\Backend\View\BackendLayout\DataProviderContext;
use TYPO3\CMS\Backend\View\BackendLayout\DataProviderInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class PackageFileDataProvider
 *
 */
class PackageFileDataProvider implements DataProviderInterface
{
    const FILE_TYPES_LAYOUT = 'ts,txt';
    const FILE_TYPES_ICON = 'png,gif,jpg';
    const FILE_TYPES_TRANSLATION = 'xlf,xml';

    /**
     * @var array
     */
    protected $packageKeys = [];

    /**
     * @var array
     */
    protected $packageKeysToExclude;

    /**
     * @var array
     */
    protected $packageCategories;

    /**
     * @var \T3easy\DistributionHelper\Domain\DataTransferObject\BackendLayoutOptions
     */
    protected $backendLayoutOptions;

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     *
     *
     * @return \T3easy\DistributionHelper\Backend\View\BackendLayout\PackageFileDataProvider
     */
    public function __construct()
    {
        /** @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface objectManager */
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        $extensionManagerConfiguration = ExtensionManagerConfigurationUtility::getConfiguration();
        $this->packageKeys = $extensionManagerConfiguration->getPackageKeys();
        $this->packageKeysToExclude = $extensionManagerConfiguration->getPackageKeysToExclude();
        $this->packageCategories = $extensionManagerConfiguration->getPackageCategories();
    }

    /**
     * Adds backend layouts to the given backend layout collection.
     *
     * @param DataProviderContext $dataProviderContext
     * @param BackendLayoutCollection $backendLayoutCollection
     * @return void
     */
    public function addBackendLayouts(DataProviderContext $dataProviderContext, BackendLayoutCollection $backendLayoutCollection)
    {
        $this->backendLayoutOptions = PageTSconfigUtility::getBackendLayoutOptionsByPageTSconfig(
                $dataProviderContext->getPageTsConfig()
        );
        $files = $this->getLayouts();
        foreach ($files as $file) {
            $backendLayout = $this->createBackendLayout($file);
            $backendLayoutCollection->add($backendLayout);
        }
    }

    /**
     * Gets a backend layout by (regular) identifier.
     *
     * @param string $identifier
     * @param int $pageId
     * @return null|BackendLayout
     */
    public function getBackendLayout($identifier, $pageId)
    {
        $this->backendLayoutOptions = PageTSconfigUtility::getBackendLayoutOptionsByPageId($pageId);
        $layouts = $this->getLayouts();
        foreach ($layouts as $layout) {
            if ($identifier === $layout['layoutIdentifier']) {
                return $this->createBackendLayout($layout);
            }
        }

        return null;
    }

    /**
     * Creates a new backend layout using the given record data.
     *
     * @param array $layout
     * @return BackendLayout
     */
    protected function createBackendLayout($layout)
    {
        $fileInformation = pathinfo($layout['file']);
        $backendLayout = BackendLayout::create(
            $layout['layoutIdentifier'],
            $layout['packageTitle'] . ' ' . $this->getTitle($layout['packageKey'], $fileInformation),
            GeneralUtility::getUrl($layout['file'])
        );

        $this->addIcon($layout['packageKey'], $fileInformation, $backendLayout);

        return $backendLayout;
    }

    /**
     * Get all files
     *
     * @throws \UnexpectedValueException
     * @return array
     */
    protected function getLayouts()
    {
        $layoutCollection = [];
        $packages = $this->getPackages();
        foreach ($packages as $packageKey => $packageInformation) {
            $path = $this->getPackagePath($packageKey);
            $filesOfDirectory = GeneralUtility::getFilesInDir($path, self::FILE_TYPES_LAYOUT, true, 1);
            foreach ($filesOfDirectory as $file) {
                $this->addLayoutToCollection(
                    $file,
                    $layoutCollection,
                    $packageKey,
                    $packageInformation['title']
                );
            }
        }

        foreach ($this->backendLayoutOptions->getFiles() as $identifier => $file) {
            $this->addLayoutToCollection(
                    GeneralUtility::getFileAbsFileName($file),
                    $layoutCollection,
                    'file_' . $identifier,
                    $GLOBALS['LANG']->sL('LLL:EXT:distribution_helper/Resources/Private/Language/locallang.xlf:file') .
                        ' ' . GeneralUtility::underscoredToUpperCamelCase($identifier)
            );
        }

        foreach ($this->backendLayoutOptions->getDirectories() as $identifier => $directory) {
            $directory = GeneralUtility::getFileAbsFileName($directory);
            $filesOfDirectory = GeneralUtility::getFilesInDir($directory, self::FILE_TYPES_LAYOUT, true, 1);
            foreach ($filesOfDirectory as $file) {
                $this->addLayoutToCollection(
                        $file,
                        $layoutCollection,
                        'directory_' . $identifier,
                        $GLOBALS['LANG']->sL('LLL:EXT:distribution_helper/Resources/Private/Language/locallang.xlf:directory') .
                            ' ' . GeneralUtility::underscoredToUpperCamelCase($identifier)
                );
            }
        }

        return $layoutCollection;
    }

    /**
     * Return packages
     *
     * @return array
     */
    protected function getPackages()
    {
        $packages = [];

        /** @var \TYPO3\CMS\Extensionmanager\Utility\ListUtility $listUtility */
        $listUtility = $this->objectManager->get('TYPO3\\CMS\\Extensionmanager\\Utility\\ListUtility');

        $availablePackages = $listUtility->getAvailableAndInstalledExtensionsWithAdditionalInformation();
        foreach ($availablePackages as $packageKey => $packageInformation) {
            if ($packageInformation['installed'] === true
                && !in_array($packageKey, $this->packageKeysToExclude)
                && (in_array($packageInformation['category'], $this->packageCategories)
                    || in_array($packageKey, $this->packageKeys)
                    || in_array($packageKey, $this->backendLayoutOptions->getPackages())
                )
            ) {
                $packages[$packageKey] = $packageInformation;
            }
        }

        return $packages;
    }

    /**
     * @param string $packageKey
     * @return null|string
     */
    protected function getPackagePath($packageKey)
    {
        $configurationPath = GeneralUtility::getFileAbsFileName(
            'EXT:' . $packageKey . '/Configuration/BackendLayouts/'
        );
        $resourcesPath = GeneralUtility::getFileAbsFileName(
            'EXT:' . $packageKey . '/Resources/Private/BackendLayouts/'
        );

        if (is_dir($configurationPath)) {
            $path = $configurationPath;
        } elseif (is_dir($resourcesPath)) {
            $path = $resourcesPath;
        } else {
            return null;
        }

        return $path;
    }

    /**
     * Add an optional icon to the BackendLayout
     *
     * @param string $packageKey
     * @param array $fileInformation
     * @param BackendLayout $backendLayout
     * @return void
     */
    protected function addIcon($packageKey, $fileInformation, BackendLayout $backendLayout)
    {
        $imageExtensions = explode(',', self::FILE_TYPES_ICON);

        if ($this->isFileOrDirectory($packageKey)) {
            $filePath = $fileInformation['dirname'] . '/' . $fileInformation['filename'];
        } else {
            $filePath = GeneralUtility::getFileAbsFileName(
                'EXT:' . $packageKey . '/Resources/Public/Icons/BackendLayouts/' . $fileInformation['filename']
            );
        }

        foreach ($imageExtensions as $extension) {
            $icon = $filePath . '.' . $extension;
            if (is_file($icon)) {
                $icon = '../' . str_replace(PATH_site, '', $icon);
                $backendLayout->setIconPath($icon);
                break;
            }
        }
    }

    /**
     *
     * @param string $packageKey
     * @param array $fileInformation pathinfo() information of the given file
     * @return string
     */
    protected function getTitle($packageKey, $fileInformation)
    {
        $title = $fileInformation['filename'];

        $translationFileEndings = explode(',', self::FILE_TYPES_TRANSLATION);

        if ($this->isFileOrDirectory($packageKey)) {
            $filePath = $fileInformation['dirname'] . '/locallang.';
        } else {
            $filePath = GeneralUtility::getFileAbsFileName(
                'EXT:' . $packageKey . '/Resources/Private/Language/locallang.'
            );
        }

        foreach ($translationFileEndings as $extension) {
            $file = $filePath . $extension;
            if (is_file($file)) {
                $file = str_replace(PATH_site, '', $file);
                $translatedTitle = $GLOBALS['LANG']->sL('LLL:' . $file . ':' . $fileInformation['filename']);
                if ($translatedTitle) {
                    $title = $translatedTitle;
                    break;
                }
            }
        }

        return $title;
    }

    /**
     * @param string $file
     * @param array $layoutCollection
     * @param string $packageKey
     * @param string $packageTitle
     * @throws \UnexpectedValueException
     * @return void
     */
    protected function addLayoutToCollection($file, array &$layoutCollection, $packageKey, $packageTitle)
    {
        $key = sha1($file);
        if (isset($layoutCollection[$key])) {
            throw new \UnexpectedValueException(sprintf('The file "%s" exists already, see "%s"', $file, $layoutCollection[$key]));
        }
        $fileInformation = pathinfo($file);
        $layoutCollection[$key]['file'] = $file;
        $layoutCollection[$key]['layoutIdentifier'] =
            GeneralUtility::underscoredToUpperCamelCase($packageKey) . '_' . $fileInformation['filename'];
        $layoutCollection[$key]['packageKey'] = $packageKey;
        $layoutCollection[$key]['packageTitle'] = $packageTitle;
    }

    /**
     * @param string $packageKey
     * @return bool
     */
    protected function isFileOrDirectory($packageKey)
    {
        if (stripos($packageKey, 'file_') === 0 || stripos($packageKey, 'directory_') === 0) {
            return true;
        }

        return false;
    }
}
