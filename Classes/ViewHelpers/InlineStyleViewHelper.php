<?php
namespace T3easy\DistributionHelper\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Jan Kiesewetter <jan@t3easy.de>, t3easy
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * ViewHelper to inline styles
 */
class InlineStyleViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'style';

    /**
     * Initialize arguments.
     *
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerTagAttribute('type', 'string', 'Specifies the media type of the style tag, default "text/css"', false, 'text/css');
        $this->registerTagAttribute('media', 'string', 'Specifies what media/device the media resource is optimized for', false, 'all');
        $this->registerTagAttribute('title', 'string', 'Specifies alternative style sheet sets', false);
    }

    /**
     * @param string $path Path of CSS file
     * @param string $position Position of inline styles, default header, "footer" or "inplace"
     *
     * @throws \TYPO3\CMS\Fluid\Core\ViewHelper\Exception
     * @return string|null
     */
    public function render($path, $position = 'header')
    {
        $position = strtolower($position);

        $path = GeneralUtility::getFileAbsFileName($path);

        if ($this->viewHelperVariableContainer->exists(__CLASS__, 'processedFiles')) {
            $processedFiles = $this->viewHelperVariableContainer->get(__CLASS__, 'processedFiles');
            if (is_array($processedFiles) && in_array($path, $processedFiles)) {
                return null;
            }
        }

        if (!is_file($path)) {
            throw new \TYPO3\CMS\Fluid\Core\ViewHelper\Exception('CSS file not found.', 1441615432);
        }

        $cacheIdentifier = sha1($path . filemtime($path));
        if (($content = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager')->getCache('distributionhelper_styles')->get($cacheIdentifier)) === false) {
            $cssMinifier = GeneralUtility::makeInstance('MatthiasMullie\\Minify\\CSS');
            $cssMinifier->add($path);
            $content = $cssMinifier->minify();

            GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager')->getCache('distributionhelper_styles')->set($cacheIdentifier, $content);
        }

        $this->tag->setContent($content);

        $processedFiles[] = $path;
        $this->viewHelperVariableContainer->addOrUpdate(__CLASS__, 'processedFiles', $processedFiles);

        if ($position !== 'inplace') {
            /** @var \TYPO3\CMS\Core\Page\PageRenderer $pageRenderer */
            $pageRenderer = $this->objectManager->get('TYPO3\\CMS\\Core\\Page\\PageRenderer');
        }
        if ($position === 'inplace') {
            return $this->tag->render();
        } elseif ($position === 'footer') {
            $pageRenderer->addFooterData($this->tag->render());
        } else {
            $pageRenderer->addHeaderData($this->tag->render());
        }

        return null;
    }
}
