<?php
namespace T3easy\DistributionHelper\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Jan Kiesewetter <jan@t3easy.de>, t3easy
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Class BackendLayoutViewHelper
 *
 */
class BackendLayoutViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * Return backend_layout of current page or backend_layout_next_level from parent or above
     *
     * @param bool $cut Just return filename of the backend layout identifier
     * @return string
     */
    public function render($cut = true)
    {
        $page = $GLOBALS['TSFE']->page;
        if ($page['backend_layout']) {
            $backendLayout = $page['backend_layout'];
        } else {
            $backendLayout = $this->getRootLineValue('backend_layout_next_level');
        }
        if ($cut) {
            $backendLayout = substr($backendLayout, strpos($backendLayout, '__') + 2);
            $backendLayout = substr($backendLayout, strpos($backendLayout, '_') + 1);
        }

        return $backendLayout;
    }

    /**
     * Returns a value from the current rootline (site) from $GLOBALS['TSFE']->tmpl->rootLine;
     *
     * @param string $field The field in the rootline record to return (a field from the pages table)
     * @return string the value from the field of the rootline
     */
    protected function getRootLineValue($field)
    {
        $rootLine = $GLOBALS['TSFE']->tmpl->rootLine;
        $key = count($rootLine) - 2;
        for ($a = $key; $a >= 0; $a--) {
            $val = $rootLine[$a][$field];
            if ($val) {
                return $val;
            }
        }
        return '';
    }
}
