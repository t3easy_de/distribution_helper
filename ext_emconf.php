<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Distribution Helper',
    'description' => 'Helper for distributions',
    'category' => 'misc',
    'author' => 'Jan Kiesewetter',
    'author_email' => 'jan@t3easy.de',
    'author_company' => 'T3easy',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '0.4.0-dev',
    'constraints' => [
        'depends' => [
            'typo3' => '6.2.15-8.7.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
];
