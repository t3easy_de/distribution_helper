﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../../Includes.txt


.. _install:

Installing the extension
------------------------

Installing the extension from Extension Manager
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Distribution Helper extension can be installed through the TYPO3 Extension Manager.

.. _download_configure:

Downloading and configuring Distribution Helper
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. Open the Extension Manager and select *Get Extensions*
#. Update the Extension-List
#. Search for *distribution_helper* and install it
#. Configure it in the Extension Manager

.. figure:: ../../Images/ExtensionManagerConfiguration.png
	:alt: Extension Manager Configuration
	:width: 850

	The Extension Manager Configuration
