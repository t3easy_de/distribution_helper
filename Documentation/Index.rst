.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
Distribution Helper
=============================================================

.. only:: html

	:Classification:
		distribution_helper

	:Version:
		|release|

	:Language:
		en

	:Description:
		Helper for distributions

	:Keywords:
		backendlayouts

	:Copyright:
		2014

	:Author:
		Jan Kiesewetter

	:Email:
		jan@t3easy.de

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 5
	:titlesonly:
	:glob:

	Introduction/Index
	AdministratorManual/Index
	DeveloperManual/Index
	ToDoList/Index

