﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _todo:

To-Do list
==========

* Add ViewHelpers for backend layout identifier
* Add inclusion of backend layouts via TypoScript / TSConfig