﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _introduction:

Introduction
============


.. _what-it-does:

What does it do?
----------------

This extension loads backend layouts from files.
It automatically includes files form extensions with category *distribution* if they are stored in
*/Configuration/BackendLayouts/*.

Icons for the layouts can be stored in */Resources/Public/Icons/BackendLayouts/* and must have the same filename as
the backend layout file.

The extension already ships some basic backend layouts:

* 33|33|33%
* 33|66%
* 50|50%
* 66|33%
* 100%

If you don't need them, you can remove the extension key *distribution_helper* from the extension manager
configuration "Add backend layouts from this extensions".

The filename is used as description and part of the identifier. You can set a more user friendly description for the
backend layout in */Resources/Private/Language/locallang.xlf* (or locallang.xml).

.. _screenshots:

Screenshots
-----------

.. figure:: ../Images/ExtensionManagerConfiguration.png
	:width: 850
	:alt: Extension Manager Configuration

	The Extension Manager Configuration

.. figure:: ../Images/PageAppearance.png
	:width: 850
	:alt: Page appearance tab

	The page appearance tab
