'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

var settings = {
	paths: {
		sass: './Resources/Private/Sass/**/*.scss',
		css: './Resources/Public/Stylesheets'
	},
	sass: {
		outputStyle: 'nested', // nested, expanded, compact, compressed
		includePaths: [
			'./node_modules/breakpoint-sass/stylesheets',
			'./node_modules/singularitygs/stylesheets'
		]
	}
};

gulp.task('sass', function () {
	gulp.src(settings.paths.sass)
			.pipe(sass(settings.sass).on('error', sass.logError))
			.pipe(gulp.dest(settings.paths.css));
});

gulp.task('sass:watch', function () {
	gulp.watch(settings.paths.sass, ['sass']);
});

gulp.task('build', function() {
	settings.sass.outputStyle = 'compressed';
	gulp.start('sass');
});

gulp.task('default', ['sass', 'copy']);
