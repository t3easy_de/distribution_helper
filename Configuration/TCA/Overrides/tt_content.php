<?php
defined('TYPO3_MODE') or die();

$GLOBALS['TCA']['tt_content']['columns']['tx_distribution_helper_classes'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:distribution_helper/Resources/Private/Language/locallang_db.xlf:tt_content.tx_distribution_helper_classes',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectCheckBox',
        'items' => [],
        'size' => 5,
        'maxitems' => 10,
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content', 'tx_distribution_helper_classes'
);
