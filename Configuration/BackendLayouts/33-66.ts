backend_layout {
	colCount = 3
	rowCount = 1
	rows {
		1 {
			columns {
				1 {
					name = LLL:EXT:distribution_helper/Resources/Private/Language/locallang.xlf:backendLayout.column.aside
					colPos = 1
				}
				2 {
					name = LLL:EXT:distribution_helper/Resources/Private/Language/locallang.xlf:backendLayout.column.main
					colspan = 2
					colPos = 0
				}
			}
		}
	}
}