<?php
$extensionPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('distribution_helper');

return [
    'MatthiasMullie\\PathConverter\\Converter' => $extensionPath . 'Resources/Private/PHP/matthiasmullie/path-converter/src/Converter.php',
    'MatthiasMullie\\Minify\\Minify' => $extensionPath . 'Resources/Private/PHP/matthiasmullie/minify/src/Minify.php',
    'MatthiasMullie\\Minify\\CSS' => $extensionPath . 'Resources/Private/PHP/matthiasmullie/minify/src/CSS.php',
    'MatthiasMullie\\Minify\\JS' => $extensionPath . 'Resources/Private/PHP/matthiasmullie/minify/src/JS.php',
    'MatthiasMullie\\Minify\\Exception' => $extensionPath . 'Resources/Private/PHP/matthiasmullie/minify/src/Exception.php',
];
